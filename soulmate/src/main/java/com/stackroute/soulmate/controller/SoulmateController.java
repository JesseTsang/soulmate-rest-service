package com.stackroute.soulmate.controller;

import com.stackroute.soulmate.domain.Soulmate;
import com.stackroute.soulmate.exception.SoulmateProfileAlreadyExistException;
import com.stackroute.soulmate.service.SoulmateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/")
public class SoulmateController {
    private SoulmateService soulmateService;

    @Autowired
    public SoulmateController(SoulmateService soulmateService) {
        this.soulmateService = soulmateService;
    }

    @PostMapping("soulmate")
    public ResponseEntity<Soulmate> createSoulmateProfile(@RequestBody Soulmate soulmate) throws SoulmateProfileAlreadyExistException {
        Soulmate createdProfile = soulmateService.createSoulmate(soulmate);

        return new ResponseEntity<>(createdProfile, HttpStatus.CREATED);
    }

    @GetMapping("soulmates")
    public ResponseEntity<List<Soulmate>> getAllSoulmateProfiles() {
        return new ResponseEntity<>(soulmateService.getAllSoulmates(), HttpStatus.OK);
    }

    @GetMapping("soulmate/{soulmateProfileID}")
    public ResponseEntity<Soulmate> getSoulmateByID(@PathVariable("soulmateProfileID") int soulmateProfileID) {
        return new ResponseEntity<>(soulmateService.getSoulmateById(soulmateProfileID), HttpStatus.OK);
    }

    @DeleteMapping("soulmate/{soulmateProfileID}")
    public ResponseEntity<Soulmate> deleteSoulmateProfile(@PathVariable("soulmateProfileID") int soulmateProfileID) {
        return new ResponseEntity<>(soulmateService.deleteSoulmate(soulmateProfileID), HttpStatus.NO_CONTENT);
    }

    @PutMapping("soulmate")
    public ResponseEntity<Soulmate> updateSoulmateProfile(@RequestBody Soulmate soulmate) {
        Soulmate updatedSoulmateProfile = soulmateService.updateSoulmate(soulmate);

        return new ResponseEntity<>(updatedSoulmateProfile, HttpStatus.OK);
    }
}
