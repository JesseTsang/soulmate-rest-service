package com.stackroute.soulmate.exception;

public class SoulmateProfileNotFoundException extends RuntimeException{
    public SoulmateProfileNotFoundException() {
    }

    public SoulmateProfileNotFoundException(Integer id) {
        super(String.format("Profile with Id %d not found.", id));
    }
}
