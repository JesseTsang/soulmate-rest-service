package com.stackroute.soulmate.exception;

public class SoulmateProfileAlreadyExistException extends RuntimeException {
    public SoulmateProfileAlreadyExistException() {
    }

    public SoulmateProfileAlreadyExistException(Integer id) {
        super(String.format("Profile with Id %d already exist.", id));
    }
}
