package com.stackroute.soulmate.repository;

import com.stackroute.soulmate.domain.Soulmate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SoulmateRepository extends CrudRepository <Soulmate, Integer> {
}
