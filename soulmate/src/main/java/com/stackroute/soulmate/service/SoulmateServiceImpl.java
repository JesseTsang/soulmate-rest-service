package com.stackroute.soulmate.service;

import com.stackroute.soulmate.domain.Soulmate;
import com.stackroute.soulmate.exception.SoulmateProfileAlreadyExistException;
import com.stackroute.soulmate.exception.SoulmateProfileNotFoundException;
import com.stackroute.soulmate.repository.SoulmateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SoulmateServiceImpl implements SoulmateService{
    @Autowired
    SoulmateRepository soulmateRepository;

    @Override
    public Soulmate createSoulmate(Soulmate soulmate) {
        boolean isProfileExist = soulmateRepository.findById(soulmate.getId()).isPresent();

        if(isProfileExist) {
            throw new SoulmateProfileAlreadyExistException(soulmate.getId());
        } else {
            return soulmateRepository.save(soulmate);
        }
    }

    @Override
    public List<Soulmate> getAllSoulmates() {
        List<Soulmate> allSoulmates = new ArrayList<>();
        soulmateRepository.findAll().forEach(allSoulmates::add);
        return allSoulmates;
    }

    @Override
    public Soulmate getSoulmateById(int soulmateID) {
        boolean isSoulmateExist = soulmateRepository.findById(soulmateID).isPresent();
        Soulmate soulmateProfile;

        if(isSoulmateExist) {
            soulmateProfile = soulmateRepository.findById(soulmateID).get();
            return soulmateProfile;
        } else {
            throw new SoulmateProfileNotFoundException(soulmateID);
        }
    }

    @Override
    public Soulmate deleteSoulmate(int soulmateID) {
        boolean isSoulmateExist = soulmateRepository.findById(soulmateID).isPresent();
        Soulmate oldSoulmateProfile;

        if(isSoulmateExist) {
            oldSoulmateProfile = soulmateRepository.findById(soulmateID).get();
            soulmateRepository.deleteById(soulmateID);

            return oldSoulmateProfile;
        } else {
            throw new SoulmateProfileNotFoundException(soulmateID);
        }
    }

    @Override
    public Soulmate updateSoulmate(Soulmate soulmate) {
        int soulmateID = soulmate.getId();
        boolean isSoulmateExist = soulmateRepository.existsById(soulmateID);

        if(isSoulmateExist) {
            soulmateRepository.save(soulmate);

            return soulmate;
        } else {
            throw new SoulmateProfileNotFoundException(soulmateID);
        }
    }
}
