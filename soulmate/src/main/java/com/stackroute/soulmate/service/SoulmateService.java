package com.stackroute.soulmate.service;

import com.stackroute.soulmate.domain.Soulmate;

import java.util.List;

public interface SoulmateService {
    Soulmate createSoulmate(Soulmate soulmate);

    List<Soulmate> getAllSoulmates();

    Soulmate getSoulmateById(int soulmateID);

    Soulmate deleteSoulmate(int soulmateID);

    Soulmate updateSoulmate(Soulmate soulmate);
}
